function createNewUser() {
    let firstName = prompt("Введіть своє ім'я:" );
    let lastName = prompt("Введіть своє прізвище:");

    let user =  {
        firstName: firstName ,
        lastName: lastName ,
        getLogin: function () {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },
        setFirstName:function (newFirstName) {
            Object.defineProperty(this, "firstName", {
                writable: false
            })
            this.firstName = newFirstName ;
        },
        setLastName:function (newLastName) {
            Object.defineProperty(this, "lastName",{
                writable:false
            });
            this.lastName = newLastName;
        }
    };
    return user ;
}

let newUser = createNewUser();
console.log(newUser.getLogin());